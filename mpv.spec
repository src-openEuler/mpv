Name:           mpv
Version:        0.36.0
Release:        3
Summary:        Movie player playing most video formats and DVDs
License:        GPL-2.0-or-later AND LGPL-2.1-or-later
URL:            http://mpv.io/
Source0:        https://github.com/mpv-player/mpv/archive/v%{version}/%{name}-%{version}.tar.gz
Patch01:        0001-add-opengl-api-for-mpv-0.35.patch
Patch02:        0001-fix-build-error-of-meson.patch
BuildRequires:  pkgconfig(alsa)
BuildRequires:  desktop-file-utils
BuildRequires:  gcc
BuildRequires:  pkgconfig(dvdnav)
BuildRequires:  pkgconfig(egl)
BuildRequires:  pkgconfig(enca)
BuildRequires:  pkgconfig(libavutil) >= 56.12.100
BuildRequires:  pkgconfig(libavcodec) >= 58.16.100
BuildRequires:  pkgconfig(libavdevice) >= 57.0.0
BuildRequires:  pkgconfig(libavformat) >= 58.9.100
BuildRequires:  pkgconfig(libswscale) >= 5.0.101
BuildRequires:  pkgconfig(libavfilter) >= 7.14.100
BuildRequires:  pkgconfig(libswresample) >= 3.0.100
BuildRequires:  pkgconfig(ffnvcodec)
BuildRequires:  pkgconfig(gbm)
BuildRequires:  pkgconfig(gl)
BuildRequires:  pkgconfig(jack)
BuildRequires:  pkgconfig(mujs)
BuildRequires:  pkgconfig(uchardet) >= 0.0.5
BuildRequires:  pkgconfig(rubberband)
BuildRequires:  pkgconfig(libguess)

BuildRequires:  pkgconfig(vulkan)

BuildRequires:  pkgconfig(lcms2)
BuildRequires:  pkgconfig(libarchive) >= 3.4.0
BuildRequires:  pkgconfig(libass)
BuildRequires:  pkgconfig(libbluray)
BuildRequires:  pkgconfig(libcdio)
BuildRequires:  pkgconfig(libcdio_paranoia)
BuildRequires:  pkgconfig(libdrm)
BuildRequires:  pkgconfig(libjpeg)
BuildRequires:  pkgconfig(libpipewire-0.3) >= 0.3.19
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libquvi-0.9)
BuildRequires:  pkgconfig(libva)
BuildRequires:  pkgconfig(luajit)

BuildRequires:  pkgconfig(sdl2)
BuildRequires:  pkgconfig(smbclient)
BuildRequires:  pkgconfig(vdpau)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-cursor)
BuildRequires:  pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-scanner)
BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(xdamage)
BuildRequires:  pkgconfig(xext)
BuildRequires:  pkgconfig(xinerama)
BuildRequires:  pkgconfig(xkbcommon)
BuildRequires:  pkgconfig(xpresent)
BuildRequires:  pkgconfig(xrandr)
BuildRequires:  pkgconfig(xscrnsaver)
BuildRequires:  pkgconfig(xv)
BuildRequires:  pkgconfig(zimg)
# Requires zimg version >= 2.9
BuildRequires:  pkgconfig(zlib)
BuildRequires:  python3-docutils
BuildRequires:  perl(Math::BigInt)
BuildRequires:  perl(Math::BigRat)
BuildRequires:  perl(Encode)
BuildRequires:  meson

# Obsoletes older ci/cd
Obsoletes:  mpv-master < %{version}-100
Provides:   mpv-master = %{version}-100

Requires:       hicolor-icon-theme
Provides:       mplayer-backend

%description
Mpv is a movie player based on MPlayer and mplayer2. It supports a wide variety
of video file formats, audio and video codecs, and subtitle types. Special
input URL types are available to read input from a variety of sources other
than disk files. Depending on platform, a variety of different video and audio
output methods are supported.

%package libs
Summary: Dynamic library for Mpv frontends 

%description libs
This package contains the dynamic library libmpv, which provides access to Mpv.

%package libs-devel
Summary: Development package for libmpv
Requires: mpv-libs%{?_isa} = %{version}-%{release}

%description libs-devel
Libmpv development header files and libraries.

%prep
%autosetup -p1 -n mpv-%{?commit}%{?!commit:%{version}}

sed -i -e "s|c_preproc.standard_includes.append('/usr/local/include')|c_preproc.standard_includes.append('$(pkgconf --variable=includedir libavcodec)')|" wscript

%build
%meson --auto-features=auto \
    -Dbuild-date=false \
    -Dlibmpv=true \
    -Dsdl2=enabled \
    -Dlibarchive=enabled \
    -Ddvdnav=enabled \
    -Dcdda=enabled \
    -Dhtml-build=enabled \
    -Ddvbin=enabled \
    -Dgl-x11=enabled \
    -Dwayland=enabled
    
%meson_build

%install
%meson_install

desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop
install -Dpm 644 README.md etc/input.conf etc/mpv.conf -t %{buildroot}%{_docdir}/%{name}/

%files
%docdir %{_docdir}/%{name}/
%{_docdir}/%{name}/
%license LICENSE.GPL LICENSE.LGPL Copyright
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%dir %{_datadir}/bash-completion/
%dir %{_datadir}/bash-completion/completions/
%{_datadir}/bash-completion/completions/%{name}
%{_datadir}/icons/hicolor/*/apps/%{name}*.*
%dir %{_datadir}/zsh/
%dir %{_datadir}/zsh/site-functions/
%{_datadir}/zsh/site-functions/_mpv
%{_mandir}/man1/%{name}.*
%{_metainfodir}/%{name}.metainfo.xml
%dir %{_sysconfdir}/%{name}/
%config(noreplace) %{_sysconfdir}/%{name}/encoding-profiles.conf

%files libs
%license LICENSE.GPL LICENSE.LGPL Copyright
%{_libdir}/libmpv.so.*

%files libs-devel
%{_includedir}/%{name}/
%{_libdir}/libmpv.so
%{_libdir}/pkgconfig/mpv.pc

%changelog
* Wed Feb 26 2025 peijiankang <peijiankang@kylinos.cn> - 0.36.0-3
- update 0001-add-opengl-api-for-mpv-0.35.patch

* Fri Jan 17 2025 peijiankang <peijiankang@kylinos.cn> - 0.36.0-2
- fix build error

* Thu Aug 15 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 0.36.0-1
- Update to 0.36.0

* Wed Apr 10 2024 peijiankang <peijiankang@kylinos.cn> - 0.35.1-2
- add 0001-add-opengl-api-for-mpv.patch

* Mon Feb 27 2023 jchzhou <zhoujiacheng@iscas.ac.cn> - 0.35.1-1
- Update to 0.35.1
- Enabled VA-API support
- Enabled Wayland backend & native PipeWire output support

* Tue Aug 10 2021 weidong <weidong@uniontech.com> - 0.32.0-2
- rename local function conflicting with pause

* Thu May 13 2021 He Rengui <herengui@uniontech.com> - 0.32.0-1
- package init
